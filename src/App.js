import React, { Component } from 'react'
import Status from './components/Status';
import './App.scss';
import ImageSec from './components/ImageSec';
import Header from './components/Header';
import Footer from './components/Footer';
// import user1 from './imgs/';
// import user2 from '../public/imgs/user-two.jpeg';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="client-demo">
          <div className="container">
            <ul className="nav nav-pills" id="pills-tab" role="tablist">
              <li className="nav-item">
                <a className="nav-link active" id="pills-home-tab" data-toggle="pill" href="#status-bx" role="tab" aria-selected="true">Status Box</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" id="pills-profile-tab" data-toggle="pill" href="#image-bx" role="tab"  aria-selected="false">Image Box</a>
              </li>
              
            </ul>
            <div className="tab-content" id="pills-tabContent">
              <div className="tab-pane fade show active" id="status-bx" role="tabpanel" >
                <Status />
              </div>
              <div className="tab-pane fade" id="image-bx" role="tabpanel" >
                <ImageSec />
              </div>
            </div>
          </div>
        </div>
        <Footer />

      </div>
    )
  }
}
export default App;
