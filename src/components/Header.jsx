import React, { Component } from 'react'

export default class Header extends Component {
    render() {
        return (
            
                <header className="navbar fixed-top navbar-dark bg-dark">
                    <a className="navbar-brand" href="#">
                        <img src="/images/d-logo.png" width="150" className="d-inline-block align-top" alt="" />
                    </a>
                </header>
            
        )
    }
}
