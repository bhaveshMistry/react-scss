import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <footer>
                <p>Copyrights © 2020 All Rights Reserved</p>
            </footer>
        )
    }
}
