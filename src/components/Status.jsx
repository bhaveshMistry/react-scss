import React, { Component } from 'react'
// import img1 from '../imgs/img-hd-1.jpg'
// import img2 from '../imgs/img-hd-2.jpg'
// import img3 from '../imgs/user-one.jpeg'
// import img4 from '../imgs/user-two.jpeg'


class Status extends Component {
    handleSubmit = (e) => {
        e.preventDefault();
        let x = document.getElementById("comment-sec")
        x.classList.add("active")
    }
    handleReply= (e) =>{
        e.preventDefault();
        let y = document.getElementById("r-box")
        y.classList.add("active")

        let q = document.getElementById("child-ans-bx")
        q.classList.remove("active")

        let p = document.getElementById("child-comment")
        p.classList.remove("active")
    }

    handleChildCommnet= (e) =>{
        e.preventDefault();
        let y = document.getElementById("child-comment")
        y.classList.add("active")

        let p = document.getElementById("child-ans-bx")
        p.classList.add("active")
        
    }

    handleReplyRemove= (e) =>{
        e.preventDefault();
        let z = document.getElementById("r-box")
        z.classList.remove("active")
    }
    handleMainReplyRemove= (e) =>{
        e.preventDefault();
        let y = document.getElementById("comment-sec")
        y.classList.remove("active")
        let z = document.getElementById("r-box")
        z.classList.remove("active")
    }

    handleLink= (e) =>{
        e.preventDefault();
        let y = document.getElementById("btn")
        y.classList.toggle("active")
    }
    handleLink1= (e) =>{
        e.preventDefault();
        let z = document.getElementById("btn1")
        z.classList.toggle("active")
    }
    handleLink2= (e) =>{
        e.preventDefault();
        let x = document.getElementById("btn2")
        x.classList.toggle("active")
    }
    
    handleLikebtn= (e) =>{
        e.preventDefault();
        let x = document.getElementById("btnLike")
        x.classList.toggle("active")
    }
    handleDisLikebtn= (e) =>{
        e.preventDefault();
        let x = document.getElementById("btnDislike")
        x.classList.toggle("active")
    }
    handleLikebtn3= (e) =>{
        e.preventDefault();
        let x = document.getElementById("btnLike3")
        x.classList.toggle("active")
    }
    handleDisLikebtn4= (e) =>{
        e.preventDefault();
        let x = document.getElementById("btnDislike4")
        x.classList.toggle("active")
    }
    
    
    render() {
        return (
            <div className="comment-box">
            
              <div className="row">
                <div className="col-12">
                  <div className="c-details">
                  <h3 className="heading-sec">Status Box</h3>
                    <div className="main-img">
                        <img src='/images/img-hd-1.jpg' alt="" />
                    </div>
                    <ul className="mi-action-bx">
                        <li id="btn">
                            <button className="btn" onClick={this.handleLink}  > <i className="fa fa-thumbs-up mr-1" aria-hidden="true"></i> Like</button>
                        </li>
                        <li  id="btn1">
                            <button className="btn" onClick={this.handleLink1}> <i className="fa fa-comments mr-1" aria-hidden="true"></i> Comment</button>
                        </li>
                        <li id="btn2">
                            <button className="btn" onClick={this.handleLink2} > <i className="fa fa-share mr-1" aria-hidden="true"></i> Share</button>
                        </li>
                    </ul>
                    
                    <div className="c-answer-bx">
                        <div className="img">
                            <img src='/images/user-one.jpeg' alt="" />
                        </div>
                        <div className="cans-desc">
                            <div className="form-group">
                                <input type="text" className="form-control" placeholder="Enter comment" />
                            </div>
                            <button id="btn-send" onClick={this.handleSubmit} className="btn btn-primary">Send</button>
                        </div>
                    </div>
                    <div className="cm-box"> 
                        <div id="comment-sec" className="c-item c-parent">
                            <div className="img">
                                <img src='/images/user-one.jpeg' alt="" />
                            </div>
                            <div className="c-desc">
                                <h4>User Name <span>4 mins ago</span></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut massa odio, lobortis ut elementum non, lobortis eu mauris. Nam eu vestibulum quam. Vivamus non sollicitudin risus. Duis id mattis nisi, non sollicitudin quam. Aliquam ornare dui vitae odio suscipit maximus. Nunc eget orci eu nulla tempus consequat. Phasellus porttitor, diam a aliquam sollicitudin, nulla tellus pellentesque risus, in semper augue augue varius lacus.</p>
                                <div className="c-actions-bx d-flex flex-wrap">
                                    <ul className="cab-left d-flex flex-wrap">
                                        <li>
                                        <button id="btnLike" className="btnLnk " onClick={this.handleLikebtn} ><i className="fa fa-thumbs-up" aria-hidden="true"></i><span className="ml-1">2</span></button>
                                        </li>
                                        <li className="mx-2">
                                        |
                                        </li>
                                        <li className="mr-4">
                                        <button id="btnDislike" className="btnLnk"  onClick={this.handleDisLikebtn}><i className="fa fa-thumbs-down" aria-hidden="true"></i><span className="ml-1">2</span></button>
                                        </li>
                                        <li className="mr-4">
                                        <button className="btnLnk" onClick={this.handleReply} ><i className="fa fa-reply mr-1" aria-hidden="true"></i> Reply</button>
                                        </li>
                                        <li>
                                        <button className="btnLnk" onClick={this.handleMainReplyRemove} ><i className="fa fa-trash mr-1" aria-hidden="true"></i>Delete</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>  
                        
                        <div id="r-box" className="reply-bx">
                            <div id="child-ans-bx" className="c-answer-bx child-ans-bx">
                                <div className="img">
                                    <img src='/images/user-one.jpeg' alt="" />
                                </div>
                                <div className="cans-desc">
                                    <div className="form-group">
                                        <input type="text" className="form-control" placeholder="Enter comment" />
                                    </div>
                                    <button id="btn-send" onClick={this.handleChildCommnet} className="btn btn-primary">Send</button>
                                </div>
                            </div>
                            <div id="child-comment" className="c-item child-comment">
                                <div className="img">
                                    <img src='/images/user-two.jpeg' alt="" />
                                </div>
                                <div className="c-desc">
                                    <h4>User Name <span>4 mins ago</span></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut massa odio,  diam a aliquam sollicitudin, nulla tellus pellentesque risus, in semper augue augue varius lacus.</p>
                                    <div className="c-actions-bx d-flex flex-wrap">
                                    <ul className="cab-left d-flex flex-wrap">
                                    <li>
                                        <button id="btnLike3" className="btnLnk " onClick={this.handleLikebtn3} ><i className="fa fa-thumbs-up" aria-hidden="true"></i><span className="ml-1">2</span></button>
                                        </li>
                                        <li className="mx-2">
                                        |
                                        </li>
                                        <li className="mr-4">
                                        <button id="btnDislike4" className="btnLnk "  onClick={this.handleDisLikebtn4}><i className="fa fa-thumbs-down" aria-hidden="true"></i><span className="ml-1">2</span></button>
                                        </li>
                                        <li className="mr-4">
                                        <button className="btnLnk"><i className="fa fa-reply mr-1" aria-hidden="true"></i> Reply</button>
                                        </li>
                                        <li>
                                        <button className="btnLnk" onClick={this.handleReplyRemove}><i className="fa fa-trash mr-1" aria-hidden="true"></i>Delete</button>
                                        </li>
                                    </ul>
                                    </div>
                                </div>
                            </div> 
                        </div>  
                    </div>
                  </div>          
                </div>
              </div>
            
          </div>
        )
    }
}

export default Status
