import React, { Component } from 'react'


class ImageSec extends Component {
    render() {
        return (
            <div className="upload-img-bx">
                <h3 className="heading-sec">Image Upload</h3>
                <h4 className="mb-3">With Image tag </h4>
                <img src='/images/img-hd-2.jpg' className="img-sec1 mb-5" />
                <h4 className="mb-3">With Background property </h4>
                <div className="img-back"></div>
            </div>
        )
    }
}
export default ImageSec
